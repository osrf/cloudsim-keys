# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/cloudsim-keys

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-archived-gh-pages/#!/osrf/cloudsim-keys

## Until May 31st 2020, the mercurial repository can be found at

https://bitbucket.org/osrf-migrated/cloudsim-keys
